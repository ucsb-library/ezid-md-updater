### Objective:

Read a CSV file with Metadata about ETDs in ADRL and inject the data into the EZID record at CDL
using the EZID API.

---

Development of this script initiated by Jira Issue: [DIGREPO-357](https://help.library.ucsb.edu/browse/DIGREPO-357)

Script is written in PHP but is run from the command line.
In other words... it is a PHP Shell script.

command line arguments should be:

1. input file (CSV file exported from Excel.)  Line endings must be LF
2. by default the script will run in No-Op mode.  To get it to really update EZID please pass in true

example of how to run this script:

`php -f ezid_updater.php ADRL_ingest_log-PARSED-all.csv`

You will need to put the EZID username and password into the config for the script to run it.  
	See lines6 & 7 of ezid-updater.php

CSV expected columns/fields are:

1. ARK
2. Aleph Sys Number
3. Author
4. Title
5. date ingested
6. permalink for Marc field 856

Things to be aware of:

* Line 10 configures what Aleph server REST api the script will query.  
* You may need to change this depending on what your data source should be.

if you see errors like:

```
-- Processing row: 156PHP Warning:  DOMDocument::loadXML(): StartTag: invalid element name in Entity, line: 1 in /Users/ilessing/Sites/ezid-md-updater/ezid-updater.php on line 188
PHP Warning:  DOMDocument::loadXML(): Opening and ending tag mismatch: datafield line 1 and unparseable in Entity, line: 1 in /Users/ilessing/Sites/ezid-md-updater/ezid-updater.php on line 188
PHP Warning:  DOMDocument::loadXML(): Opening and ending tag mismatch: record line 1 and datafield in Entity, line: 1 in /Users/ilessing/Sites/ezid-md-updater/ezid-updater.php on line 188
PHP Warning:  DOMDocument::loadXML(): Opening and ending tag mismatch: get-record line 1 and record in Entity, line: 1 in /Users/ilessing/Sites/ezid-md-updater/ezid-updater.php on line 188
PHP Warning:  DOMDocument::loadXML(): Extra content at the end of the document in Entity, line: 1 in /Users/ilessing/Sites/ezid-md-updater/ezid-updater.php on line 188
PHP Notice:  Undefined variable: return in /Users/ilessing/Sites/ezid-md-updater/ezid-updater.php on line 218
```

It is likely due to an Aleph bug that outputs invalid XML when called from the REST API.  Records throwing this sort of error will have to be updated via EZID's web interface.

