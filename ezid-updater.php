#!/usr/bin/php
<?php
/* There should be a ReadMe.md accompanying this file which will explain it in more detail. */

// Config Variables
$ezidUN = '';
$ezidPW = '';
$ezid_login_url = "https://ezid.cdlib.org/login";
date_default_timezone_set('America/Los_Angeles');
define ("ALEPHSERVER", "pegasus-test.library.ucsb.edu");

// MODE should be blank on PRODUCTION. MODE should be 'debug' to produce extra output
define ("MODE", "");
// end config variables

// check that an input file was passed in on the command line
if($argc == 1){
	exit("\n exiting with no action. No input file specified.\n");
}
$inputfile = $argv[1];
echo "\nInput file: " . $inputfile . "\n";

$noop = false;
if($argc == 2){
	// missing second parameter on command line so run in no-op mode
	$noop = true;
	echo "\n running in No Op mode\n";
	}
if($argc > 2){
	if(strtolower($argv[2]) != "true"){
		// second command line parameter is not "true" so run in no-op mode
		$noop = true;
		echo "\n running in No Op mode\n";
		}
	}

$handle = fopen($inputfile, "r");
if($handle === false){ 	exit("\n Unable to open input file. Exiting.\n"); }

// Login to EZID and obtain session cookie
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $ezid_login_url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_USERPWD, "$ezidUN:$ezidPW");
curl_setopt($ch, CURLOPT_HEADER, true);

$output = curl_exec($ch);
if(curl_getinfo($ch, CURLINFO_HTTP_CODE) !== 200){
	exit("EZID Authentication failed.  \n\n");
}else{
	echo "EZID Authentication succeeded.\n";
}
curl_close($ch);
preg_match_all('|Set-Cookie: (.*);|U', $output, $results);
$autheticated_ezid_session_cookie = implode(';', $results[1]);

//	iterate over all lines of input file
$row = 1;
while(($data = fgetcsv($handle, 1000, ",")) !== FALSE){
echo("\n-- Processing row: $row");
/*  Expected cols:
		1. ARK
		2. Aleph Sys Number
		3. Author
		4. Title
		5. date ingested
		6 permalink for Marc field 856
		*/

	$row++;

//	make function calls to gather and format data
	$zod = get_ezid_info($data[0], $autheticated_ezid_session_cookie);
	$record = ezid_2array($zod);
	if(!empty($data[1])){
		$aleph_data =  aleph_data($data[1]);
	}else{
		// we have no aleph system number
		echo "no aleph system number";
		continue;
	}

// Set the URL associated with the ID
$input ="_target: http://alexandria.ucsb.edu/lib/".$data[0]."\n";

// Set the Who: field associated with the ID
$input .="erc.who: ".$aleph_data[0]."\n";

// Set the What: field associated with the ID
$input .="erc.what: ".$aleph_data[1]."\n";

// Set the When: field associated with the ID
$input .="erc.when: ".$aleph_data[2]."\n";

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "http://ezid.cdlib.org/id/".$data[0]);
curl_setopt($ch, CURLOPT_COOKIE, $autheticated_ezid_session_cookie);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-length: '.strlen($input), 'Content-Type: text/plain; charset=UTF-8'));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_TIMEOUT, 5);
if($noop != true){
		curl_setopt($ch, CURLOPT_POSTFIELDS, $input);
	}else{
		curl_setopt($ch, CURLOPT_POSTFIELDS, "");
		echo "\nRunning in No Op mode: input not sent:\n\n". $input . "\n\n";
	}
curl_setopt($ch, CURLOPT_HEADER, true); //TRUE to include the header in the output.  equiv to -i on the command line
curl_setopt($ch, CURLINFO_HEADER_OUT, true);
$headers_sent = curl_getinfo($ch, CURLINFO_HEADER_OUT);

$output = curl_exec($ch);
if(curl_getinfo($ch, CURLINFO_HTTP_CODE) !== 200){
		echo("Error: EZID call failed HTTP code ".curl_getinfo($ch, CURLINFO_HTTP_CODE)." near line ". __LINE__ ."\n");
		echo("Error: returned from EZID: \n");
		echo($output);
	}else{
		if(MODE == 'debug'){
			echo "EZID call succeeded. line: ". __LINE__ ."\n";
		}
	}
}// end while - iterating over all lines of input file

curl_close($ch);
echo "\n";

// ========= Functions =======================================================
// transform the string of data that EZID outputs into an array.
function ezid_2array( $ezid_data ){
	$ezid_data = trim($ezid_data);
	$lines = explode("\n", $ezid_data);
	$line1 = array_shift($lines);
	$ark = substr($line1, 9, strlen($line1) - 9);
	
	foreach ($lines as $key => $value) {
		$temp = explode(':', $value, 2); 
		$retkey = trim($temp[0], '_');
		$retval = trim($temp[1]);
		$return[$retkey] = $retval;
	}
    return $return;
}

// retrieve data from EZID for a given ARK ID
function get_ezid_info($ark, $autheticated_ezid_session_cookie){
	$url = 'http://ezid.cdlib.org/id/'.$ark;
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_COOKIE, $autheticated_ezid_session_cookie);
	$output = curl_exec($ch);
	if(curl_getinfo($ch, CURLINFO_HTTP_CODE) !== 200){
		exit("Error retrieving data from EZID" . $url ."\n");
	}
	curl_close($ch);
	return $output;
}

// retrieve published date from ADRL for a given ARK
function adrl_created_date($ark){
/*This will require two http calls:
	1 to get a session cookie
	example URL:  http://alexandria.ucsb.edu/lib/ark:/48907/f3m043c2
	2 to get the XML data
	example URL:  http://alexandria.ucsb.edu/welcome/content?cu=1	
*/	
}


// retrieve data from Aleph/Pegasus for a given system number
function aleph_data($alephID){
	// example URL: http://pegasus-test.library.ucsb.edu:1891/rest-dlf/record/SBA01004077014?view=full
	$url = "http://". ALEPHSERVER . ":1891/rest-dlf/record/SBA01" . $alephID . "?view=full";
	if(MODE == 'debug'){
		echo "\nDebug line " . __LINE__ . ". Calling to URL: " . $url . "\n"; //debug
	}

        //setting the curl parameters.
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $marcxmldata = curl_exec($ch);
            if($marcxmldata === false){
            	exit( "Curl error: " . curl_error($ch)."\n");
            }
	        curl_close($ch);
		$record_doc = new DomDocument();
		$record_doc->loadXML($marcxmldata);
		if(MODE == 'debug'){echo "\nXML from Aleph:\n" . $marcxmldata . "\n" ;}//debug
		$xpath = new DOMXpath($record_doc);
		// get the author from MARC 100a field
		$xpathquery ="//datafield[@tag='100']/subfield[@code='a']";
		$xpathresult = $xpath->query($xpathquery);
		foreach ($xpathresult as $key => $value) {
			$return[] = trim($value->nodeValue, ".");
		}
		// get the title from MARC 245a
				$xpathquery ="//datafield[@tag='245']/subfield[@code='a']";
		$xpathresult = $xpath->query($xpathquery);
		foreach ($xpathresult as $key => $value) {
			$return[] = trim($value->nodeValue, "./");
		}
		// get the date from MARC 264c field
		$xpathquery ="//datafield[@tag='264']/subfield[@code='c']";
		$xpathresult = $xpath->query($xpathquery);
		foreach ($xpathresult as $key => $value) {
			$return[] = trim($value->nodeValue, ".");
		}
		if(MODE == 'debug'){
			echo "This is what is being ruturned from the aleph_data() function:\n";
			print_r($return);
		}

	return $return;
}

// parse the data from aleph to build a full title string from MARC record fields 245a and 245b 
function aleph_full_title($aleph_data){
	$title = $aleph_data[1];
	if(isset($aleph_data[2])){
		$subtitle = trim($aleph_data[2], "/");
	}
	if(isset($subtitle)){
		$return = $title ." : ".$subtitle;
	}else{
		$return = $title;
	}
	return $return;
}

